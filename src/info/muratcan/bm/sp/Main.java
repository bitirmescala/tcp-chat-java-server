package info.muratcan.bm.sp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Main {

	public static final int SERVER_PORT = 6226;
	
	static ArrayList<ChatServer> clientList=new ArrayList<ChatServer>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ServerSocket serverSocket = null;

		try {
			serverSocket = new ServerSocket(SERVER_PORT);
			while (true) {
				Socket newClient=serverSocket.accept();
				ChatServer newChatServer=new ChatServer(newClient);
				clientList.add(newChatServer);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void broadcastMsg(String msg){
		for(ChatServer c:clientList){
			c.sendMessage(msg);
		}
	}
	
	public static String getConnected(){
		String names="";
		for(ChatServer c:clientList){
			names=names+", "+c.getClientName();
		}
		names=names.substring(2);
		return names;
	}
	
	public static void deleteClient(ChatServer c){
		broadcastMsg("Client "+c.getClientName()+" has left!");
		clientList.remove(c);
	}

}
