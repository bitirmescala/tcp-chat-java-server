package info.muratcan.bm.sp;

import java.net.*;
import java.io.*;

public class ChatServer extends Thread {
	protected Socket clientSocket;
	String clientName="nobody";
	PrintWriter out;
	BufferedReader in;
	

	public ChatServer(Socket clientSoc) {
		System.out.println("New Client Connected!");
		clientSocket = clientSoc;
		start();
	}

	public void run() {
		try {
			System.out.println("Client Thread Started!");
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			
			out.println("You are connected to java version of server!");
			out.print("Choose a name:");
			out.flush();
			
			String inMsg;
			
			if((inMsg = in.readLine()) != null){
				clientName=inMsg;
				out.println("SERVER: Welcome "+clientName+".");
				Main.broadcastMsg(clientName+" has joined.");
				out.println("SERVER: Connected users: "+Main.getConnected() +".");
			}
			
			while ((inMsg = in.readLine()) != null) {
				System.out.println("Client: " + inMsg);
				Main.broadcastMsg(clientName+": "+inMsg);
			}

			out.close();
			in.close();
			clientSocket.close();
			Main.deleteClient(this);
			System.out.println("Client left!");
		} catch (IOException e) {
			
		}
	}
	
	public void sendMessage(String msg){
		out.println(msg);
	}

	public String getClientName() {
		return clientName;
	}
}
